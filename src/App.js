
import './sass/style.scss';
import StartScreen from './components/StartScreen'
import GameScreen from './components/GameScreen'
import { Provider } from 'react-redux'
import store from './redux/store'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

function App() {
  return (
    <Provider store={store}>
      <Router basename={process.env.PUBLIC_URL}>  
      <Routes>
        <Route path="/" element={<StartScreen />} />
        <Route path="/game-screen" element={<GameScreen />} />
      </Routes>
      </Router>
    </Provider>
  );
}

export default App;