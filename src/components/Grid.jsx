import React, { useState, useEffect, useRef } from 'react'
import { connect } from 'react-redux'

import { updatePlayerGrid } from './../redux/actionCreators'

const Grid = ({shipsRotation, playerGrid, updatePlayerGrid, shipRef}) => {

    const grid = useRef()
    const [boxes, setBoxes] = useState([])
    const carrierHorizontalProhibition = [8, 9, 10, 18, 19, 20, 28, 29, 30, 38, 39, 40, 48, 49, 50, 58, 59, 60, 68, 69, 70, 78, 79, 80, 88, 89, 90, 98, 99, 100]
    const carrierVerticalProhibition = [71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93 ,94, 95, 96, 97, 98, 99, 100] 
    const cruiserHorizontalProhibition = [9, 10, 19, 20, 29, 30, 39, 40, 49, 50, 59, 60, 69, 70, 79, 80, 89, 90, 99, 100]
    const cruiserVerticalProhibition = [81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93 ,94, 95, 96, 97, 98, 99, 100] 
    const submarineHorizontalProhibition = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
    const submarineVerticalProhibition = [91, 92, 93 ,94, 95, 96, 97, 98, 99, 100] 

    const getGridBoxClass = (box) => {
        if(box.filled && box.hitted) return "grid__box--hitted"
        if(box.filled && !box.hitted) return "grid__box--filled"
        if(!box.filled && box.hitted) return "grid__box--failed"
        return "grid__box"
    }
    
    return (
        <div 
            className="grid" 
            ref={grid}
            onDragStart={(e) => {
                e.dataTransfer.setData("text/plain", e.target.id)
            }}
            onDragOver={(e) => {
                e.preventDefault()
            }}
            onDrop={(e) => {
                e.preventDefault()
                const ship = e.dataTransfer.getData("text")
                switch (ship) {
                    case "carrier":
                        if(shipsRotation === "horizontal"){
                            if(!carrierHorizontalProhibition.includes(parseInt(e.target.id))){
                                let newBoxes = playerGrid.slice()
                                newBoxes[parseInt(e.target.id)].filled = true
                                newBoxes[parseInt(e.target.id) + 1].filled = true
                                newBoxes[parseInt(e.target.id) + 2].filled = true
                                newBoxes[parseInt(e.target.id) + 3].filled = true
                                updatePlayerGrid(newBoxes)
                                shipRef.current.remove()
                            }
                        }
                        else {
                            if(!carrierVerticalProhibition.includes(parseInt(e.target.id))){
                                let newBoxes = playerGrid.slice()
                                newBoxes[parseInt(e.target.id)].filled = true
                                newBoxes[parseInt(e.target.id) + 10].filled = true
                                newBoxes[parseInt(e.target.id) + 20].filled = true
                                newBoxes[parseInt(e.target.id) + 30].filled = true
                                updatePlayerGrid(newBoxes)
                                shipRef.current.remove()
                            }
                        }
                        break;

                        case "cruiser":
                            if(shipsRotation === "horizontal"){
                                if(!cruiserHorizontalProhibition.includes(parseInt(e.target.id))){
                                    let newBoxes = playerGrid.slice()
                                    newBoxes[parseInt(e.target.id)].filled = true
                                    newBoxes[parseInt(e.target.id) + 1].filled = true
                                    newBoxes[parseInt(e.target.id) + 2].filled = true
                                    updatePlayerGrid(newBoxes)
                                    shipRef.current.remove()
                                }
                            }
                            else {
                                if(!cruiserVerticalProhibition.includes(parseInt(e.target.id))){
                                    let newBoxes = playerGrid.slice()
                                    newBoxes[parseInt(e.target.id)].filled = true
                                    newBoxes[parseInt(e.target.id) + 10].filled = true
                                    newBoxes[parseInt(e.target.id) + 20].filled = true
                                    updatePlayerGrid(newBoxes)
                                    shipRef.current.remove()
                                }
                            }
                            break;

                            case "submarine":
                                if(shipsRotation === "horizontal"){
                                    if(!submarineHorizontalProhibition.includes(parseInt(e.target.id))){
                                        let newBoxes = playerGrid.slice()
                                        newBoxes[parseInt(e.target.id)].filled = true
                                        newBoxes[parseInt(e.target.id) + 1].filled = true
                                        updatePlayerGrid(newBoxes)
                                        shipRef.current.remove()
                                    }
                                }
                                else {
                                    if(!submarineVerticalProhibition.includes(parseInt(e.target.id))){
                                        let newBoxes = playerGrid.slice()
                                        newBoxes[parseInt(e.target.id)].filled = true
                                        newBoxes[parseInt(e.target.id) + 10].filled = true
                                        updatePlayerGrid(newBoxes)
                                        shipRef.current.remove()
                                    }
                                }
                                break;
                }
            }}
        >
            {
                playerGrid.map((el, key) => (
                    <div className={getGridBoxClass(el)} key={key} id={key}>
            
                    </div>
                ))
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        shipsRotation: state.shipsRotation,
        playerGrid: state.playerGrid
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatePlayerGrid: (grid) => { dispatch(updatePlayerGrid(grid)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid)