import React, { useRef } from 'react'
import { connect } from 'react-redux'
import { rotateShips } from './../redux/actionCreators'

const Ships = ({shipsRotation, rotateShips, shipRef, shipContainer}) => {

    return (
        <div 
            className="ships"
            onDragStart={(e) => {
                e.dataTransfer.setData("text/plain", e.target.type)
                shipRef.current = e.target
            }}
        >
            <ul className="ships__list" ref={shipContainer}>
                <li 
                    className={shipsRotation === "horizontal" ? "ships__list-item-1" : "ships__list-item-1 ships__list-item-1--rotate"} 
                    draggable="true"
                    id="carrier"
                    type="carrier"
                >
                </li>
                <li 
                    className={shipsRotation === "horizontal" ? "ships__list-item-2" : "ships__list-item-2 ships__list-item-2--rotate"} 
                    draggable="true"
                    id="cruiser1"
                    type="cruiser"
                >
                </li>
                <li 
                    className={shipsRotation === "horizontal" ? "ships__list-item-3" : "ships__list-item-3 ships__list-item-3--rotate"} 
                    draggable="true"
                    id="cruiser2"
                    type="cruiser"
                >
                </li>
                <li 
                    className={shipsRotation === "horizontal" ? "ships__list-item-4" : "ships__list-item-4 ships__list-item-4--rotate"} 
                    draggable="true"
                    id="cruiser3"
                    type="cruiser"
                >
                </li>
                <li 
                    className={shipsRotation === "horizontal" ? "ships__list-item-5" : "ships__list-item-5 ships__list-item-5--rotate"} 
                    draggable="true"
                    id="submarine"
                    type="submarine"
                >
                </li>
            </ul>
            <button className="ships__rotate" onClick={() => { rotateShips() }}>Rotar</button>
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        shipsRotation: state.shipsRotation
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        rotateShips: () => { dispatch(rotateShips()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Ships)