import React, { useState, useRef } from 'react'
import Ships from './Ships'
import Grid from './Grid'
import { Navigate } from 'react-router-dom'
import { connect } from 'react-redux'
import { addPlayerName } from './../redux/actionCreators'

const StartScreen = ({addPlayerName}) => {

    const shipRef = useRef([])
    const shipContainer = useRef()
    const [redirect, setRedirect] = useState(false)

    const handleSubmit = (e) => {
        e.preventDefault()
        if(!shipContainer.current.hasChildNodes()){
            setRedirect(true)
            addPlayerName(e.target.name.value)
        }
        else {
            alert("Necesitas colocar tus naves primero")
        }
    }

    return redirect ? <Navigate to="/game-screen" /> : (
        <>
            <Ships shipRef={shipRef} shipContainer={shipContainer}/>
            <Grid shipRef={shipRef}/>
            <form className="form" onSubmit={handleSubmit}>
                <label htmlFor="name">Nombre del jugador</label>
                <input type="text" id="name" required/>
                <input type="submit" value="Comenzar"/>
            </form>
        </>
    )
}

const mapDispatchToProps = (dispatch) => {
    return {
        addplayerName: (playerName) => { dispatch(addPlayerName(playerName)) }
    }
}

export default connect(null, mapDispatchToProps)(StartScreen)