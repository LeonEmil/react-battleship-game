import React from 'react'
import { connect } from 'react-redux'
import { updateCpuGrid, setPlayerReady } from './../redux/actionCreators'

const CpuGrid = ({cpuGrid, updateCpuGrid, playerReady, setPlayerReady}) => {

    const getGridBoxClass = (box) => {
        if(box.filled && box.hitted) return "grid__box--hitted"
        if(!box.filled && box.hitted) return "grid__box--failed"
        return "grid__box grid__box--cpu"
    }

    const handleClickBox = (key) => {
        if(playerReady){
            let newCpuGrid = cpuGrid.slice()
            if(newCpuGrid[key].filled){
                newCpuGrid[key].hitted = true
                updateCpuGrid(newCpuGrid)
            }
            if(!newCpuGrid[key].filled){
                newCpuGrid[key].hitted = true
                updateCpuGrid(newCpuGrid)
            }
            setPlayerReady(false)
        }
    }

    return (
        <div className="grid">
            {
                cpuGrid.map((box, key) => {
                    return (
                        <div className={getGridBoxClass(box)} onClick={() => { handleClickBox(key) }} key={key}>
                        </div>
                    )
                })
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        cpuGrid: state.cpuGrid,
        playerReady: state.playerReady
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setPlayerReady: (boolean) => { dispatch(setPlayerReady(boolean)) },
        updateCpuGrid: (newCpuGrid) => { dispatch(updateCpuGrid(newCpuGrid)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CpuGrid)