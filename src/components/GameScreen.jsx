import React, { useState, useEffect } from 'react'
import PlayerGrid from './PlayerGrid'
import CpuGrid from './CpuGrid'
import EndScreen from './EndScreen'
import { connect } from 'react-redux'
import { updatePlayerGrid, updateCpuGrid } from './../redux/actionCreators'
import { playerGrid as defaultPlayerGrid, cpuGrid as defaultCpuGrid } from './../js/grids'

const GameScreen = ({playerGrid, cpuGrid, updateplayerGrid, updateCpuGrid, playerName}) => {

    const [redirect, setRedirect] = useState(false)
    const [gameWinner, setGameWinner] = useState("")

    useEffect(() => {
        let playerHits = playerGrid.reduce((ac, el) => {
            if(el.filled && el.hitted){
                ac = ac + 1
            }
            return ac
        }, 0)
        if(playerHits >= 15){
            setRedirect(true)
            setGameWinner("CPU")
            updatePlayerGrid(defaultPlayerGrid)
            updateCpuGrid(defaultCpuGrid)
        }
        
        let cpuHits = cpuGrid.reduce((ac, el) => {
            if(el.filled && el.hitted){
                ac = ac + 1
            }
            return ac
        }, 0)
        if(cpuHits >= 15){
            setRedirect(true)
            setGameWinner(playerName)
            updatePlayerGrid(defaultPlayerGrid)
            updateCpuGrid(defaultCpuGrid)

        }
    })

    return redirect ? <EndScreen gameWinner={gameWinner} /> : (
        <>
            <PlayerGrid />
            <CpuGrid />
        </>
    )
}

const mapStateToProps = (state) => {
    return {
        playerGrid: state.playerGrid,
        cpuGrid: state.cpuGrid,
        playerName: state.playerName
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatePlayerGrid: (playerGrid) => { dispatch(updatePlayerGrid(playerGrid) ) },
        updateCpuGrid: (cpuGrid) => { dispatch(updateCpuGrid(cpuGrid)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameScreen) 