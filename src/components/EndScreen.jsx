import React from 'react'

const EndScreen = ({gameWinner}) => {
    return (
        <div className="end-screen">
            <h1>{gameWinner} ha ganado!</h1>
            <button onClick={() => { window.location = "/" }}>Volver a jugar</button>
        </div>
    )
}

export default EndScreen