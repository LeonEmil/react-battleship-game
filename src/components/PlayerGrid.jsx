import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { setPlayerReady, updatePlayerGrid } from './../redux/actionCreators'

const PlayerGrid = ({playerGrid, updatePlayerGrid, playerReady, setPlayerReady}) => {

    useEffect(() => {
        if(!playerReady){
            let newPlayerGrid = playerGrid.slice()
            newPlayerGrid[Math.floor(Math.random() * 100)].hitted = true
            updatePlayerGrid(newPlayerGrid)
            setPlayerReady(true)
        }
    })

    const getGridBoxClass = (box) => {
        if(box.filled && box.hitted) return "grid__box--hitted"
        if(box.filled && !box.hitted) return "grid__box--filled"
        if(!box.filled && box.hitted) return "grid__box--failed"
        return "grid__box"
    }

    return (
        <div className="grid">
            {
                playerGrid.map((box, key) => {
                    return (
                        <div className={getGridBoxClass(box)} key={key}>
                        </div>
                    )
                })
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        playerGrid: state.playerGrid,
        playerReady: state.playerReady
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updatePlayerGrid: (playerGrid) => { dispatch(updatePlayerGrid(playerGrid)) },
        setPlayerReady: (boolean) => { dispatch(setPlayerReady(boolean)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlayerGrid)