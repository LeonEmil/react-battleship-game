const ADD_PLAYER_NAME = "ADD_PLAYER_NAME"
const ROTATE_SHIPS = "ROTATE_SHIPS"
const CREATE_PLAYER_GRID = "CREATE_PLAYER_GRID"
const UPDATE_PLAYER_GRID = "UPDATE_PLAYER_GRID"
const CREATE_CPU_GRID = "CREATE_CPU_GRID"
const UPDATE_CPU_GRID = "UPDATE_CPU_GRID"
const SET_PLAYER_READY = "SET_PLAYER_READY"

export {
    ADD_PLAYER_NAME,
    ROTATE_SHIPS,
    CREATE_PLAYER_GRID,
    CREATE_CPU_GRID,
    UPDATE_PLAYER_GRID,
    UPDATE_CPU_GRID,
    SET_PLAYER_READY
}