import * as actions from './actionTypes'
import { playerGrid, cpuGrid } from './../js/grids'

const initialState = {
    playerName: "Jugador",
    playerGrid: playerGrid.slice(),
    cpuGrid: cpuGrid.slice(),
    shipsRotation: 'horizontal',
    playerReady: true
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.ADD_PLAYER_NAME: 
            return {
                ...state,
                playerName: action.playerName
            } 

        case actions.SET_PLAYER_READY:
            return {
                ...state,
                playerReady: action.playerReady
            }

        case actions.UPDATE_PLAYER_GRID:
            return {
                ...state,
                playerGrid: action.playerGrid
            }

        case actions.ROTATE_SHIPS:
            return {
                ...state,
                shipsRotation: state.shipsRotation === "horizontal" ? "vertical" : "horizontal"
            }
    
        default: return state
    }
}

export default reducer
