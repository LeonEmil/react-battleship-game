import * as actions from './actionTypes'

const addPlayerName = (playerName) => {
    return {
        type: actions.ADD_PLAYER_NAME,
        playerName: playerName
    }
}

const setPlayerReady = (playerReady) => {
    return {
        type: actions.SET_PLAYER_READY,
        playerReady: playerReady
    }
}

const updatePlayerGrid = (grid) => {
    return {
        type: actions.UPDATE_PLAYER_GRID,
        playerGrid: grid
    }
}

const updateCpuGrid = (grid) => {
    return {
        type: actions.UPDATE_CPU_GRID,
        cpuGrid: grid
    }
}

const rotateShips = () => {
    return {
        type: actions.ROTATE_SHIPS,
    }
}

export { 
    addPlayerName,
    setPlayerReady, 
    rotateShips,
    updatePlayerGrid,
    updateCpuGrid
}
